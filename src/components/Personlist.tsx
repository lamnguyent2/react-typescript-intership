import {Name} from './Person.types'

type PersonlistProps = {
    names: Name[]
};
export const Personlist = (props: PersonlistProps) => {
    return (
        <div>
            {props.names.map(name => {
                return(
                    <h2 key={name.first} > {name.last}</h2>
                )
            })}
        </div>
    );
};