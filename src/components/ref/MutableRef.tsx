import { useEffect, useRef, useState } from "react";

export const MutableRef = () => {
    const [timer, setTimer] = useState(0)
    const interValRef = useRef<number | null>(null)

    const stoptimer = () => {
        if(interValRef.current){
            window.clearInterval(interValRef.current)
        }
    }

    useEffect(()=>{
        interValRef.current = window.setInterval(() => {
            setTimer((timer) => timer +1)
        }, 1000)
        return () => {
            stoptimer()
        }
    })

    return (
        <div>
            HookTimer - {timer} - 
            <button onClick={()=>stoptimer()}>Stop Timer</button>
        </div>
    );
};