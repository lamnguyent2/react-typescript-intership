import './App.css';
import { Button } from './components/Button';
import { Container } from './components/Container';

import { ThemeContextProvider } from './components/context/ThemeContext';
import { UserContextProvider } from './components/context/UserContext';
import { User } from './components/context/User';
import { Greet } from './components/Greet';
import { Heading } from './components/Heading';
import { Input } from './components/Input';
import { Oscar } from './components/Oscar';
import { Person } from './components/Person';
import { Personlist } from './components/Personlist';
import { Status } from './components/Status';
import { Box } from './components/context/Box';
import { Counter } from './components/class/Counter';
import { Private } from './components/auth/Private';
import { Profile } from './components/auth/Profile';
import { List } from './components/generics/List';
import { RandomNumber } from './components/restriction/RandomNumber';
import { Toast } from './components/templateliterrals/Toast';
import { CustomButton } from './components/html/Button';
import { Text } from './components/polymorphic/Text';
// import { Lists } from './components/generics/List';

function App() {
  const personName ={
    first: "Bruce",
    last: "Wayne"
  }

  const nameList = [
    {
      first: 'Bruce',
      last: 'Wayne'
    },
    {
      first: 'Clark',
      last: 'Kent'
    },
    {
      first: 'Princess',
      last: 'Diana'
    }
  ] 

  return (
    <div className="App">
      <h1>Basic Props</h1>
      <Person name={personName} />
      <Personlist names={nameList} />
      <br /><br />

      <h1>Advanced Props</h1>
      <Status status='loading' />
      <Heading>Placeholder text</Heading>
      <Oscar><Heading>Oscar goes to Leonardo Dicpario!</Heading></Oscar>
      <br /><br />

      <h1>Typing Props</h1>
      <Greet name='Vishwas'  isLoggedIn ={false} />
      <br /><br />

      <h1>Event Props</h1>
      <Button handleClick={(event, id) => {
        console.log('Button clicked', event, id)
      }} 
      />
      <Input value='' handleChange={event => console.log(event)} />
      <br /><br />

      <h1>Style Props</h1>
      <Container styles={{border: '1px solid black', padding: '1rem'}} />
      <br /><br />

      <h1>useContext Hook</h1>
      <ThemeContextProvider>
        <Box />
      </ThemeContextProvider>
      <br /><br />

      <h1>useContext Future Value</h1>
      <UserContextProvider>
        <User />
      </UserContextProvider>
      <br /><br />

      <h1>Class Component</h1>
      <Counter message='The count value is ' />
      <br /><br />

      <h1>Component Prop</h1>
      <Private isLoggedIn={true} component={Profile} />
      <br /><br />

      <h1>Generic Props</h1>
      <List items={['Batman', 'Superman', 'Wonder Woman']} onClick={(item)=>console.log(item)} />
      <List items={[1, 2, 3]} onClick={(item)=>console.log(item)} />
      {/* <Lists items={[{id: 1,first: 'Bruce', last: 'Wayne'},{id: 2,first: 'Clark', last: 'Kent'},{id: 3,first: 'Princess', last: 'Diana'}]} onClick={(item)=>console.log(item)} /> */}
      <br /><br />

      <h1>Restricting Props</h1>
      <RandomNumber value={10} isPositive />
      <br /><br />

      <h1>Template Literals and Exclude</h1>
      <Toast position='left-center' />
      <br /><br />

      <h1>Wrapping HTML Elements</h1>
      <CustomButton variant='primary' onClick={()=>console.log('Clicked')}>Primary Button</CustomButton>
      <br /><br />

      <h1>Polymorphic Components</h1>
      <Text as='h2' size='lg'>Heading</Text>
      <Text as='p' size='md'>Paragraph</Text>
      <Text as='label' htmlFor='someId' size='sm' color='secondary'>Label</Text>
    </div>
  );
}

export default App;
